# hrt-monitor

Scrapes the [Boots pharmacy stock checker](https://www.boots.com/online/psc/) to find medication, in particular
transdermal patches for transfeminine hormone therapy.

## Usage

You'll need Rust installed ([install here](https://rustup.rs/)), as well as a standard C toolchain. Then, run

```
$ cargo build --release
```

in this repository to build the program.

Once the program is built, run it at `target/release/hrt-monitor` to get usage instructions:

```
$ target/release/hrt-monitor -h
```

You'll need the latitude and longitude of a place to search in, as well as a radius in miles.
You can get this from Google Maps by [following these instructions](https://support.google.com/maps/answer/18539).

Example usage to find Estradot 75 patches within a 10 mile radius of London's City Hall:

```
$ target/release/hrt-monitor --lat 51.5073 --lon 0.0163 --radius-mi 10 --estradot 75
```

You can also search for any arbitrary medication by its SNOMED CT code. You can find this by
running a product search on the [dictionary of medicines and devices (dm+d)](https://dmd-browser.nhsbsa.nhs.uk/)
webpage; once you've found the page for the product you want, scroll down and copy off the SNOMED code.

For example, Amfexa 5mg dexamfetamine tablets (a medication for ADHD) have the SNOMED code
`30742411000001106`, so you would try:

```
$ target/release/hrt-monitor --lat 51.5073 --lon 0.0163 --radius-mi 10 --snomed 30742411000001106
```

## FAQ

- What should I put in the SNOMED CT browser?
  - Try specifying the exact brand name and dose of the medication you want -- e.g. "Amfexa 5mg" rather than just
    "dexamfetamine".
- What are these "(tripped the firewall, waiting 1/10...)" messages about?
  - Boots have a [WAF](https://en.wikipedia.org/wiki/Web_application_firewall) from Incapsula to try and
    prevent automated scraping (which is what we're doing here). When it blocks us, the program will wait
    for a bit before trying again, which seems to work well in my tests.
  - It might help to navigate to the [stock checker](https://www.boots.com/online/psc/) in your actual web browser
    when this happens.
