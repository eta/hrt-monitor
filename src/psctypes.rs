//! JSON response bodies from the Boots Prescription Stock Checker API.

use serde::de::Error;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

fn deserialize_yes_no<'de, D>(deserializer: D) -> Result<bool, D::Error>
where
    D: serde::de::Deserializer<'de>,
{
    let st: String = String::deserialize(deserializer)?;
    match &st as &str {
        "Y" => Ok(true),
        "N" => Ok(false),
        x => Err(D::Error::custom(format!("Y/N was {x}"))),
    }
}

#[derive(Deserialize, Debug, Clone, Copy)]
pub enum StockIndication {
    #[serde(rename = "G")]
    InStock,
    #[serde(rename = "A")]
    LowStock,
    #[serde(rename = "R")]
    OutOfStock,
}

#[derive(Deserialize, Debug, Clone)]
pub struct StoreLocation {
    pub latitude: f64,
    pub longitude: f64,
    #[serde(rename = "propertyEasting")]
    pub easting: String,
    #[serde(rename = "propertyNorthing")]
    pub northing: String,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct StoreAddress {
    pub administrative_area: String,
    pub country_code: String,
    pub county: Option<String>,
    #[serde(rename = "gridLocation")]
    pub location: StoreLocation,
    #[serde(default)]
    pub locality: Option<String>,
    pub postcode: String,
    pub street: Option<String>,
    pub town: Option<String>,
}

#[derive(Deserialize, Debug, Clone)]
pub struct StoreService {
    #[serde(deserialize_with = "deserialize_yes_no")]
    pub active: bool,
    pub text: String,
}

#[derive(Deserialize, Debug, Clone)]
pub struct StoreContactDetails {
    #[serde(default)]
    pub phone: Option<String>,
    #[serde(flatten)]
    pub others: HashMap<String, Value>,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct StoreInfo {
    #[serde(rename = "Address")]
    pub address: StoreAddress,
    #[serde(rename = "Services")]
    #[serde(default)]
    pub services: Option<Vec<StoreService>>,
    #[serde(default)]
    pub contact_details: Option<StoreContactDetails>,
    pub delivery_chain: String,
    #[serde(rename = "displayname")]
    pub display_name: String,
    pub name: String,
    #[serde(rename = "type")]
    pub store_type: String,
    pub store_number: usize,
    pub id: usize,
    #[serde(deserialize_with = "deserialize_yes_no")]
    pub is_franchise: bool,
    #[serde(deserialize_with = "deserialize_yes_no")]
    pub is_midnight_pharmacy: bool,
    #[serde(deserialize_with = "deserialize_yes_no")]
    pub is_pharmacy: bool,
    pub is_prescription_store_collection_available: bool,
    #[serde(deserialize_with = "deserialize_yes_no")]
    pub is_store: bool,
    #[serde(flatten)]
    pub extra_fields: HashMap<String, Value>,
}

#[derive(Deserialize, Debug, Clone)]
pub struct StoreSearchResult {
    #[serde(rename = "Location")]
    pub store: StoreInfo,
}

#[derive(Deserialize, Debug, Clone)]
pub struct StoreSearchResults {
    pub latitude: f64,
    pub longitude: f64,
    pub offset: usize,
    #[serde(rename = "radius")]
    pub radius_mi: usize,
    pub size: usize,
    pub total: usize,
    pub results: Vec<StoreSearchResult>,
    #[serde(flatten)]
    pub extra_fields: HashMap<String, Value>,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct StockLevel {
    #[serde(deserialize_with = "serde_aux::field_attributes::deserialize_number_from_string")]
    pub store_id: usize,
    pub product_id: String,
    #[serde(rename = "stockLevel")]
    pub in_stock: StockIndication,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct StockRejectedFilter {
    pub id: String,
    pub filter_type: String,
    pub reason: String,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct StockResults {
    #[serde(default)]
    pub stock_levels: Option<Vec<StockLevel>>,
    #[serde(default)]
    pub rejected_filters: Option<Vec<StockRejectedFilter>>,
}

#[derive(Serialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct StockQuery {
    pub(crate) store_id_list: Vec<usize>,
    pub(crate) product_id_list: Vec<String>,
}
