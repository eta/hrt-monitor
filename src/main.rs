mod psctypes;

use crate::psctypes::{
    StockIndication, StockLevel, StockQuery, StockResults, StoreSearchResult, StoreSearchResults,
};
use anyhow::{anyhow, bail, Context, Result};
use isahc::cookies::CookieJar;
use isahc::http::{Method, StatusCode, Version};
use isahc::prelude::*;
use isahc::{Body, Request, Response};
use rand::Rng;
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};
use std::io::Read;
use std::thread;
use std::time::Duration;
use clap::Parser;

#[derive(Parser, Debug)]
#[command(version)]
struct Args {
    /// Latitude to search for pharmacies near to
    #[arg(long, allow_hyphen_values = true)]
    lat: f64,
    /// Longitude to search for pharmacies near to
    #[arg(long, allow_hyphen_values = true)]
    lon: f64,
    /// Radius in miles for the pharmacy search
    #[arg(short, long)]
    radius_mi: usize,
    /// Which strength Estradot to search for (e.g. 25, 37.5, 50...)
    #[arg(long)]
    estradot: Option<String>,
    /// A SNOMED CT code of a drug to search for
    #[arg(long)]
    snomed: Option<String>,
    /// Whether to print debugging information.
    #[arg(short, long, action)]
    verbose: bool,
}

/// Contains the NHS SNOMED CT codes for various estrogen preparations.
#[allow(dead_code)]
mod estrogen {
    pub const ESTRADOT_25: &str = "9044911000001109";
    pub const ESTRADOT_37_5: &str = "9045111000001105";
    pub const ESTRADOT_50: &str = "9045311000001107";
    pub const ESTRADOT_75: &str = "9045511000001101";
    pub const ESTRADOT_100: &str = "9045711000001106";
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct PharmacySummary {
    pub id: usize,
    pub name: String,
    pub postcode: String,
    pub lat: f64,
    pub lon: f64,
    pub has_store_collection: bool,
    pub phone: Option<String>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct StockSummary {
    pub in_stock_ids: HashSet<usize>,
    pub low_stock_ids: HashSet<usize>,
    pub out_of_stock_ids: HashSet<usize>,
}

pub struct BootsApi {
    cookie_jar: CookieJar,
    debug: bool,
}

impl BootsApi {
    const API_ROOT: &'static str = "https://www.boots.com/online/psc";
    const USER_AGENT: &'static str = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36";

    #[allow(clippy::new_without_default)]
    pub fn new(debug: bool) -> Self {
        Self {
            cookie_jar: CookieJar::new(),
            debug
        }
    }

    pub fn api_call(&self, method: Method, uri: &str, body: Option<String>) -> Result<Response<Body>> {
        let request = Request::builder()
            .method(method)
            .uri(uri)
            .automatic_decompression(true)
            .header("Accept-Encoding", "gzip")
            .header("User-Agent", Self::USER_AGENT)
            .cookie_jar(self.cookie_jar.clone())
            .version(Version::HTTP_2)
            .header("Referer", "https://www.boots.com/online/psc");

        let request = if let Some(b) = body {
            request.header("Content-Type", "application/json")
                .body(Body::from(b))
                .context("making HTTP request")?
        }
        else {
            request.body(Body::empty())
                .context("making HTTP request")?
        };

        if self.debug {
            eprintln!("--> {request:?}");
        }

        let mut resp = request
            .send()
            .context("making HTTP request")?;

        if resp.status() != StatusCode::OK {
            let mut body_bytes = Vec::new();
            resp.body_mut()
                .read_to_end(&mut body_bytes)
                .context("reading response body")?;
            bail!(
                "HTTP status code {}: {}",
                resp.status(),
                String::from_utf8_lossy(&body_bytes)
            );
        }
        Ok(resp)
    }

    pub fn store_search(
        &self,
        lat: f64,
        lon: f64,
        radius_mi: usize,
        offset: usize,
    ) -> Result<StoreSearchResults> {
        let uri = format!("{}/search/store?type=geo&radius={radius_mi}&from={offset}&latitude={lat}&longitude={lon}", Self::API_ROOT);
        let mut resp = self.api_call(Method::GET, &uri, None)?;
        let ret = resp.json().context("deserializing response")?;
        Ok(ret)
    }

    pub fn stock_check(
        &self,
        pharmacies: Vec<usize>,
        medication_snomed_id: &str,
    ) -> Result<StockResults> {
        let uri = format!("{}/itemStock", Self::API_ROOT);
        let mut resp = self.api_call(
            Method::POST,
            &uri,
            Some(serde_json::to_string(&StockQuery {
                store_id_list: pharmacies,
                product_id_list: vec![medication_snomed_id.to_string()],
            })?),
        )?;
        let ret = resp.json().context("deserializing response")?;
        Ok(ret)
    }

    pub fn get_pharmacies_in_area(
        &self,
        lat: f64,
        lon: f64,
        radius_mi: usize,
    ) -> Result<Vec<PharmacySummary>> {
        let mut total = 0;
        let mut offset = 0;
        let mut ret: Vec<PharmacySummary> = vec![];
        let mut attempts = 0;
        while offset < total || total == 0 {
            attempts += 1;
            let results = match self.store_search(lat, lon, radius_mi, offset) {
                Ok(v) => v,
                Err(e) if e.to_string().contains("Incapsula") && attempts < 10 => {
                    eprintln!("(tripped the firewall, waiting {attempts}/10...)");
                    thread::sleep(Duration::from_millis(
                        rand::thread_rng().gen_range(30000..65000),
                    ));
                    continue;
                }
                Err(e) => {
                    return Err(e);
                }
            };
            attempts = 0;
            total = results.total;
            offset += results.size;
            for StoreSearchResult { store } in results.results {
                if !store.is_pharmacy {
                    continue;
                }
                assert_eq!(store.id, store.store_number);
                ret.push(PharmacySummary {
                    id: store.id,
                    name: store.display_name,
                    postcode: store.address.postcode,
                    lat,
                    lon,
                    has_store_collection: store.is_prescription_store_collection_available,
                    phone: store.contact_details.map(|x| x.phone).flatten(),
                });
            }
            eprintln!("(pharmacy search: {}/{})", offset, total);
            thread::sleep(Duration::from_millis(
                rand::thread_rng().gen_range(1500..3500),
            ));
        }
        Ok(ret)
    }

    pub fn get_medication_stock(
        &self,
        pharmacies: Vec<usize>,
        medication_snomed_id: &str,
    ) -> Result<StockSummary> {
        let mut ret = StockSummary {
            in_stock_ids: Default::default(),
            low_stock_ids: Default::default(),
            out_of_stock_ids: Default::default(),
        };
        let mut attempts = 0;
        // Yes, it really has to be exact, or else we get a 503
        let mut chunks = pharmacies.chunks_exact(10);
        let mut done = 0;
        let mut remainder = if chunks.remainder().is_empty() {
            None
        } else {
            Some(chunks.remainder())
        };
        let mut current_chunk = chunks.next();
        let mut sigh = vec![];
        eprintln!("(checking stock at {} pharmacies)", pharmacies.len());
        while let Some(c) = current_chunk {
            thread::sleep(Duration::from_millis(
                rand::thread_rng().gen_range(1500..3500),
            ));
            attempts += 1;
            let results = match self.stock_check(c.into(), medication_snomed_id) {
                Ok(v) => v,
                Err(e) if e.to_string().contains("Incapsula") && attempts < 10 => {
                    eprintln!("(tripped the firewall, waiting {attempts}/10...)");
                    thread::sleep(Duration::from_millis(
                        rand::thread_rng().gen_range(30000..65000),
                    ));
                    continue;
                }
                Err(e) => {
                    return Err(e);
                }
            };
            if let Some(rf) = results.rejected_filters {
                for f in rf {
                    match &f.filter_type as &str {
                        "productId" => bail!("rejected product {} for reason {}", f.id, f.reason),
                        "storeId" => {
                            eprintln!("\nwarning: store {} rejected for reason {}", f.id, f.reason)
                        }
                        _ => bail!(
                            "rejected filter type {} of {} for reason {}",
                            f.filter_type,
                            f.id,
                            f.reason
                        ),
                    }
                }
            }
            attempts = 0;
            for StockLevel {
                store_id,
                product_id,
                in_stock,
            } in results
                .stock_levels
                .ok_or_else(|| anyhow!("no stock levels in response"))?
            {
                done += 1;
                assert_eq!(product_id, medication_snomed_id);
                let dest = if let StockIndication::InStock | StockIndication::LowStock = in_stock {
                    &mut ret.in_stock_ids
                } else {
                    &mut ret.out_of_stock_ids
                };
                dest.insert(store_id);
                if let StockIndication::LowStock = in_stock {
                    ret.low_stock_ids.insert(store_id);
                }
            }
            current_chunk = chunks.next();
            if current_chunk.is_none() {
                // Hackily overwrite the thing, lol
                if let Some(r) = remainder.take() {
                    sigh.extend(c.to_owned());
                    for (i, pid) in r.iter().enumerate() {
                        sigh[i] = *pid;
                    }
                    current_chunk = Some(&sigh);
                }
            }
            eprintln!("(stock check: {done}/{})", pharmacies.len());
        }
        Ok(ret)
    }
}

fn stock_check(
    api: &BootsApi,
    pharmacies: &HashMap<usize, PharmacySummary>,
    pharmacy_ids: Vec<usize>,
    code: &str,
) -> Result<()> {
    eprintln!("[+] Checking stock for {}", code);
    let stock = api.get_medication_stock(pharmacy_ids, code)?;
    eprintln!(
        "\n[+] Found stock in {} pharmacies (out of stock in {})",
        stock.in_stock_ids.len(),
        stock.out_of_stock_ids.len()
    );
    for pid in stock.in_stock_ids.iter() {
        if let Some(p) = pharmacies.get(pid) {
            let low_text = if stock.low_stock_ids.contains(pid) {
                " [LOW STOCK]"
            } else {
                ""
            };
            let phone = if let Some(ref phone) = p.phone {
                format!(" / {phone}")
            } else {
                String::new()
            };
            eprintln!(
                "* #{}:{} {} -- {}{}",
                p.id, low_text, p.name, p.postcode, phone
            );
        }
    }
    eprintln!();
    Ok(())
}

fn main() -> Result<()> {
    let args = Args::parse();
    let snomed: &str = match (args.snomed.as_deref(), args.estradot.as_deref()) {
        (Some(s), None) => s,
        (None, Some("25")) => estrogen::ESTRADOT_25,
        (None, Some("37.5")) => estrogen::ESTRADOT_37_5,
        (None, Some("50")) => estrogen::ESTRADOT_50,
        (None, Some("75")) => estrogen::ESTRADOT_75,
        (None, Some("100")) => estrogen::ESTRADOT_100,
        (None, Some(_)) => bail!("unknown estradot type"),
        (Some(_), Some(_)) => bail!("both estradot and snomed provided"),
        (None, None) => bail!("provide one of --estradot or --snomed")
    };

    let api = BootsApi::new(args.verbose);
    eprintln!("[+] Doing pharmacy search");
    let pharmacies = api.get_pharmacies_in_area(args.lat, args.lon, args.radius_mi)?;
    eprintln!("[+] Found {} pharmacies", pharmacies.len());
    let pharmacy_ids = pharmacies.iter().map(|x| x.id).collect::<Vec<_>>();
    let pharmacies_ht = pharmacies
        .iter()
        .map(|x| (x.id, x.clone()))
        .collect::<HashMap<_, _>>();
    stock_check(
        &api,
        &pharmacies_ht,
        pharmacy_ids.clone(),
        snomed
    )?;
    Ok(())
}
